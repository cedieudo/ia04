package utils

import (
	"fmt"
	"math/rand"
	"reflect"
	"time"

	types "gitlab.utc.fr/cedieudo/ia04/agt/types"
	models "gitlab.utc.fr/cedieudo/ia04/comsoc/models"
)

// Renvoie l'indice ou se trouve alt dans prefs
func Rank(alt models.Alternative, prefs []models.Alternative) int {
	var rank int = -1

	for index, element := range prefs {
		if element == alt {
			rank = index
		}
	}

	return rank
}

// Renvoie vrai ssi alt1 est préférée à alt2
func IsPref(alt1, alt2 models.Alternative, prefs []models.Alternative) bool {
	return Rank(alt1, prefs) < Rank(alt2, prefs)
}

// Renvoie la valeur max de la map count
func Max(count models.Count) (max int) {
	max = count[0]

	for _, element := range count {
		if element > max {
			max = element
		}
	}

	return
}

// Renvoie les meilleures alternatives pour un décomtpe donné
func MaxCount(count models.Count) (bestAlts []models.Alternative) {
	bestAlts = make([]models.Alternative, 0)

	for index, element := range count {
		if element == Max(count) {
			bestAlts = append(bestAlts, models.Alternative(index))
		}
	}

	return
}

// Vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque alternative n'apparaît qu'une seule fois par préférences
func CheckProfile(prefs models.Profile) error {
	length := len(prefs[0])

	for index, element := range prefs {
		if len(element) != length {
			return fmt.Errorf("la préférence %d n'est pas complète", index)
		}
	}

	for index, element := range prefs {
		for _, alt := range element {
			if !isUnique(alt, element) {
				return fmt.Errorf("l'alternative %d apparaît plusieurs fois dans la préférence %d", alt, index)
			}
		}
	}

	return nil
}

func EmptyField(req interface{}) bool {
	if req == nil {
		return false
	}
	v := reflect.ValueOf(req)
	if v.Kind() != reflect.Struct {
		return false
	}
	for i := 0; i < v.NumField(); i++ {
		if v.Field(i).IsZero() {
			return true
		}
	}
	return false
}

func FieldEmpty(s interface{}, name string) bool {
	r := reflect.ValueOf(s)
	f := reflect.Indirect(r).FieldByName(name)
	return f.IsZero()
}

func Equals(slice1 interface{}, slice2 interface{}) bool {
	s1 := reflect.ValueOf(slice1)
	s2 := reflect.ValueOf(slice2)

	if s1.Kind() != reflect.Slice || s2.Kind() != reflect.Slice {
		return false
	}

	if s1.Len() != s2.Len() {
		return false
	}

	for i := 0; i < s1.Len(); i++ {
		if s1.Index(i).Interface() != s2.Index(i).Interface() {
			return false
		}
	}

	return true
}

// Contains method for generic slice of generic elements
func Contains(slice interface{}, value interface{}) bool {
	s := reflect.ValueOf(slice)

	if s.Kind() != reflect.Slice {
		panic("First parameter should be a slice")
	}

	for i := 0; i < s.Len(); i++ {
		if reflect.DeepEqual(s.Index(i).Interface(), value) {
			return true
		}
	}

	return false
}

func HasVoted(currentBallot types.StorableBallot, id string) bool {
	return currentBallot.Voters[id]
}

func ValidVote(request types.Request, currentBallot types.StorableBallot, profile models.Profile) (bool, string) {
	if FieldEmpty(request, "Options") && (currentBallot.Rule == "stv" || currentBallot.Rule == "approval") {
		return false, "first case"
	}

	if EmptyField(request) && !(FieldEmpty(request, "Options") && (currentBallot.Rule != "stv" && currentBallot.Rule != "approval")) {
		return false, "second case"
	}

	if len(request.Preference) != currentBallot.NumberAlts {
		st := fmt.Sprintf("third case: %d != %d", len(request.Preference), currentBallot.NumberAlts)
		return false, st
	}

	if currentBallot.Rule == "approval" && len(request.Options) != len(request.Preference) {
		st := fmt.Sprintf("fourth case: %d != %d", len(request.Options), len(request.Preference))
		return false, st
	}

	// copy profile in newProfile and append request's preference to it
	newProfile := make(models.Profile, len(profile))
	copy(newProfile, profile)
	newProfile = append(newProfile, request.Preference)

	// check if the new profile is valid
	err := CheckProfile(newProfile)
	return err == nil, "fifth case"
}

func RandomPreference(max int) []models.Alternative {
	pref := make([]models.Alternative, max)
	usedNumber := make(map[models.Alternative]bool)
	for i := 0; i < max; i++ {
		random := models.Alternative(rand.Intn(max) + 1)
		for usedNumber[random] {
			random = models.Alternative(rand.Intn(max) + 1)
		}
		pref[i] = random
		usedNumber[random] = true

		usedNumber[pref[i]] = true
	}
	return pref
}

// Vérifie qu'une alternative est unique dans une préférence
func isUnique(alt models.Alternative, prefs []models.Alternative) bool {
	count := 0

	for _, element := range prefs {
		if element == alt {
			count++
		}
	}

	return count == 1
}

// Prend un count en paramètre et renvoie une map {1: [toutes les alternatives dont le score est 1], 2: [toutes les alternatives dont le score est 2], ...}
func TieBreaksInCount(count models.Count) (equals map[int][]models.Alternative) {
	equals = make(map[int][]models.Alternative)

	for index, element := range count {
		equals[element] = append(equals[element], models.Alternative(index))
	}

	return
}

func TieBreakFactory(order []models.Alternative) func(pref []models.Alternative) (models.Alternative, error) {
	return func(equals []models.Alternative) (models.Alternative, error) {
		if len(equals) == 0 {
			return 0, fmt.Errorf("la préférence est vide")
		}

		bestAlt := equals[0]

		// En fait, dans ma conception, equals est un tableau contenant toutes les alternatives qui ont un score égal dans une méthode de vote donnée.
		// On départage toutes ces alternatives avec l'ordre strict fourni en paramètre.
		for i := 0; i < len(equals); i++ {
			if IsPref(equals[i], bestAlt, order) {
				bestAlt = equals[i]
			}
		}

		return bestAlt, nil
	}
}

func DeadlineReached(deadline string) bool {
	deadlineTime, _ := time.Parse("2006-01-02 15:04:05", deadline)
	return time.Now().Format("2006-01-02 15:04:05") > deadlineTime.Format("2006-01-02 15:04:05")
}

func FitlerProfile(profile *models.Profile, maxCount []models.Alternative) {
	for _, preference := range *profile {
		for _, alternative := range preference {
			// if the alternative is not in the maxCount, we remove it from the profile
			if !AlternativeInArray(alternative, maxCount) {
				*profile = RemoveAlternativeFromProfile(profile, alternative)
			}
		}
	}
}

func AlternativeInArray(alt models.Alternative, alternatives []models.Alternative) bool {
	for _, alternative := range alternatives {
		if alternative == alt {
			return true
		}
	}
	return false
}

func RemoveAlternativeFromProfile(profile *models.Profile, alt models.Alternative) models.Profile {
	for i := 0; i < len(*profile); i++ {
		for j := 0; j < len((*profile)[i]); j++ {
			if (*profile)[i][j] == alt {
				(*profile)[i] = append((*profile)[i][:j], (*profile)[i][j+1:]...)
			}
		}
	}
	return *profile
}

func SWFFactory(swf func(profile models.Profile) (models.Count, error), tbSWF func(order []models.Alternative) (models.Alternative, error)) func(models.Profile) (models.Count, error) {
	return func(p models.Profile) (models.Count, error) {
		count, err := swf(p)
		tieBreaks := TieBreaksInCount(count)
		result := make(map[int]models.Alternative) // {score 1 : alternative x a été choisie, score 2 : alternative y a été choisie, ...}

		for score, tieBreakSlice := range tieBreaks {
			result[score], err = tbSWF(tieBreakSlice)
		}

		for score, tieBreakSlice := range tieBreaks {
			for _, alternative := range tieBreakSlice {
				if result[score] != alternative {
					delete(count, alternative)
				}
			}
		}

		return count, err
	}
}

func SCFFactory(scf func(profile models.Profile) ([]models.Alternative, error), tbSCF func(order []models.Alternative) (models.Alternative, error)) func(models.Profile) (models.Alternative, error) {
	return func(p models.Profile) (models.Alternative, error) {
		bestAlts, err := scf(p) //Toutes les best alts sont en tie break par construction, et cela sur le meilleur score.
		bestAlt, _ := tbSCF(bestAlts)

		return bestAlt, err
	}
}

func SWFFactoryOptions(swf func(profile models.Profile, options []int) (models.Count, error), tbSWF func(order []models.Alternative) (models.Alternative, error)) func(models.Profile, []int) (models.Count, error) {
	return func(p models.Profile, options []int) (models.Count, error) {
		count, err := swf(p, options)
		tieBreaks := TieBreaksInCount(count)
		result := make(map[int]models.Alternative) // {score 1 : alternative x a été choisie, score 2 : alternative y a été choisie, ...}

		for score, tieBreakSlice := range tieBreaks {
			result[score], err = tbSWF(tieBreakSlice)
		}

		for score, tieBreakSlice := range tieBreaks {
			for _, alternative := range tieBreakSlice {
				if result[score] != alternative {
					delete(count, alternative)
				}
			}
		}

		return count, err
	}
}

func SCFFactoryOptions(scf func(profile models.Profile, options []int) ([]models.Alternative, error), tbSCF func(order []models.Alternative) (models.Alternative, error)) func(models.Profile, []int) (models.Alternative, error) {
	return func(p models.Profile, options []int) (models.Alternative, error) {
		bestAlts, err := scf(p, options) //Toutes les best alts sont en tie break par construction, et cela sur le meilleur score.
		bestAlt, _ := tbSCF(bestAlts)

		return bestAlt, err
	}
}
