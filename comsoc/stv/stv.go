package stv

import (
	"gitlab.utc.fr/cedieudo/ia04/comsoc/majoritesimple"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/models"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/utils"
)

func StvSWF(profile models.Profile) (count models.Count, err error) {
	count = make(map[models.Alternative]int)
	err = utils.CheckProfile(profile)

	// S'il y a majorité stricte, on s'arrête
	majS, _ := majoritesimple.MajoritySCF(profile)
	if len(majS) != 0 {
		return majoritesimple.MajoritySWF(profile)
	}

	for _, element := range profile {
		count[element[0]]++
	}

	return
}

func StvSCF(profile models.Profile, nbr []int) (bestAlts []models.Alternative, err error) {
	nbRound := nbr[0]
	stv, err := StvSWF(profile)

	maxCount := utils.MaxCount(stv)
	if nbRound == len(profile[0])-1 || len(maxCount) == 1 {
		return maxCount, err
	}
	// on ne garde que les candidats qui ont le maxCount dans le profile.
	// c'est à dire qu'on supprime les candidats qui n'ont pas le maxCount du profile :
	utils.FitlerProfile(&profile, maxCount)
	return StvSCF(profile, []int{nbRound + 1})
}
