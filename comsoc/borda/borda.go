package borda

import (
	"gitlab.utc.fr/cedieudo/ia04/comsoc/models"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/utils"
)

func BordaSWF(profile models.Profile) (count models.Count, err error) {
	count = make(map[models.Alternative]int)
	err = utils.CheckProfile(profile)

	for _, element := range profile {
		for i := 0; i < len(element); i++ {
			count[element[i]] += len(element) - i - 1
		}
	}

	return
}

// profile = [
// 	[1, 2, 3, 4] => 1 prend len-1 points, 2 prends len-2 points, 3 len-3, 4 len-4
// 	[3, 2, 1, 4]
// 	[2, 1, 3, 4]
// ]

func BordaSCF(p models.Profile) (bestAlts []models.Alternative, err error) {
	majority, err := BordaSWF(p)
	return utils.MaxCount(majority), err
}
