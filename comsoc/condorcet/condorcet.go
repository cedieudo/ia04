package condorcet

import (
	"gitlab.utc.fr/cedieudo/ia04/comsoc/models"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/utils"
)

func CondorcetWinner(profile models.Profile) ([]models.Alternative, error) {
	var winner models.Alternative

	err := utils.CheckProfile(profile)

	for _, alternative := range profile[0] {
		// others is the list of all alternatives except current one
		others := make([]models.Alternative, 0)
		for _, other := range profile[0] {
			if other != alternative {
				others = append(others, other)
			}
		}
		if BeatsAll(alternative, others, profile) {
			winner = alternative
			return []models.Alternative{winner}, err
		}
	}
	return []models.Alternative{}, err
}

func Beats1v1(a, b models.Alternative, profile models.Profile) bool {
	points := make(map[models.Alternative]int)
	for _, preference := range profile {
		if utils.IsPref(a, b, preference) {
			points[a]++
		} else {
			points[b]++
		}
	}
	return points[a] > points[b]
}

func BeatsAll(a models.Alternative, others []models.Alternative, profile models.Profile) (res bool) {
	res = true
	for i := 0; i < len(others); i++ {
		res = res && Beats1v1(a, others[i], profile)
	}
	return
}
