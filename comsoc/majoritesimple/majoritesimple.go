package majoritesimple

import (
	"gitlab.utc.fr/cedieudo/ia04/comsoc/models"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/utils"
)

func MajoritySWF(profile models.Profile) (count models.Count, err error) {
	count = make(map[models.Alternative]int)
	err = utils.CheckProfile(profile)

	for _, element := range profile {
		count[element[0]]++
	}

	return
}

// count = [
// 	[1, 2, 3, 4] : 2
// 	[3, 2, 1, 4] : 3
// 	[2, 1, 3, 4] : 2
// ]

func MajoritySCF(p models.Profile) (bestAlts []models.Alternative, err error) {
	majority, err := MajoritySWF(p)
	return utils.MaxCount(majority), err
}
