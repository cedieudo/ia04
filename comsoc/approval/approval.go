package approval

import (
	"gitlab.utc.fr/cedieudo/ia04/comsoc/models"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/utils"
)

// Attention, j'ai mal compris l'utilisation de approval. C'est un tableau de int pour le threshold, voir page wikipedia pour son utilisation et refaire SWF/SCF
func ApprovalSWF(profile models.Profile, threshold []int) (count models.Count, err error) {
	// for each slice of alternatives in the profile, we add 1 to the count of all alternatives.
	// But, we have to keep in mind that there is a threshold that we shouldn't exceed for all alternatives.
	// For example, if the threshold is {2, 1, 2}, that means that for each slice of alternative we inspect, the alternative that is in first position can't be counted more than 2 times.
	// The alternative that is in second position can't be counted more than 1 time, and the alternative that is in third position can't be counted more than 2 times.

	// We initialize the count map
	err = utils.CheckProfile(profile)
	count = make(map[models.Alternative]int)
	for _, preference := range profile {
		for i := 0; i < len(preference); i++ {
			if count[preference[i]] < threshold[i] {
				count[preference[i]]++
			}
		}
	}
	return
}

func ApprovalSCF(p models.Profile, threshold []int) (bestAlts []models.Alternative, err error) {
	approval, err := ApprovalSWF(p, threshold)
	return utils.MaxCount(approval), err
}
