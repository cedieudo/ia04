package copeland

import (
	"gitlab.utc.fr/cedieudo/ia04/comsoc/condorcet"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/models"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/utils"
)

func CopelandSWF(profile models.Profile) (count models.Count, err error) {
	count = make(map[models.Alternative]int)
	err = utils.CheckProfile(profile)

	for i := 0; i < len(profile[0]); i++ {
		total := 0
		for j := 0; j < len(profile[0]); j++ {
			if i != j && condorcet.Beats1v1(profile[0][i], profile[0][j], profile) {
				total += 1
			} else if i != j && condorcet.Beats1v1(profile[0][j], profile[0][i], profile) {
				total -= 1
			}
		}
		count[profile[0][i]] += total
	}

	return
}

func CopelandSCF(p models.Profile) (bestAlts []models.Alternative, err error) {
	majority, err := CopelandSWF(p)
	return utils.MaxCount(majority), err
}
