package test

import (
	"fmt"
	"log"
	"testing"
	"time"

	"gitlab.utc.fr/cedieudo/ia04/agt/ballotagent"
	"gitlab.utc.fr/cedieudo/ia04/agt/commanderagent"
	"gitlab.utc.fr/cedieudo/ia04/agt/types"
	"gitlab.utc.fr/cedieudo/ia04/agt/voteragent"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/models"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/utils"
)

// testing the api

func TestNewBallot(t *testing.T) {
	const url1 = ":8080"
	const url2 = "http://localhost:8080"
	servAgt := ballotagent.NewRestServerAgent(url1)

	log.Println("démarrage du serveur...")
	go servAgt.Start()

	// ----------------- TEST NOT IMPLEMENTED -----------------

	log.Println("démarrage du commandant...")
	id := "commander"
	url := url2
	ballot := types.Ballot{
		Rule: "plurality",
		Deadline: func() string {
			return time.Now().Add(2 * time.Minute).Format("2006-01-02 15:04:05")
		}(),
		VoterIDs:   []string{"agt1", "agt2", "agt3", "agt4", "agt5"},
		NumberAlts: 5,
	}
	cmd := commanderagent.NewCommanderAgent(
		id,
		url,
		ballot,
	)

	// send the request
	_, err := cmd.DoNewBallot()

	// return code should be NotImplemented as "plurality" is not an implemented rule
	if err.Error() != "[501] 501 Not Implemented" {
		t.Error("Error should be NotImplemented")
	}

	// ----------------- TEST BAD REQUEST -----------------

	ballot = types.Ballot{
		Rule: "copeland",
		Deadline: func() string {
			return time.Now().Add(2 * time.Minute).Format("2006-01-02 15:04:05")
		}(),
		NumberAlts: 5,
	}
	cmd = commanderagent.NewCommanderAgent(
		id,
		url,
		ballot,
	)

	// send the request
	_, err = cmd.DoNewBallot()
	cmd.DoNewBallot()

	// return code should be BadRequest as there is no agent running
	if err.Error() != "[400] 400 Bad Request" {
		t.Error("Error should be BadRequest")
	}

	// ----------------- TEST OK -----------------

	ballot = types.Ballot{
		Rule: "copeland",
		Deadline: func() string {
			return time.Now().Add(2 * time.Minute).Format("2006-01-02 15:04:05")
		}(),
		VoterIDs:   []string{"agt1", "agt2", "agt3", "agt4", "agt5"},
		NumberAlts: 5,
	}
	cmd = commanderagent.NewCommanderAgent(
		id,
		url,
		ballot,
	)

	// send the request
	res, err := cmd.DoNewBallot()

	// return code should be OK
	if err != nil {
		t.Error("Error should be nil")
	}

	// res should be a valid ballot response
	if res.BallotID == "" {
		t.Error("BallotID should not be empty")
	}
}

// func test doVote
func TestDoVote(t *testing.T) {
	// ----------------- TEST SERVICE NOT AVAILABLE -----------------
	const url1 = ":8080"
	const url2 = "http://localhost:8080"
	servAgt := ballotagent.NewRestServerAgent(url1)

	log.Println("démarrage du serveur...")
	go servAgt.Start()

	log.Println("démarrage du commandant...")
	id := "commander"
	url := url2
	ballot := types.Ballot{
		Rule:       "borda",
		Deadline:   "2006-01-02 15:04:05",
		VoterIDs:   []string{"agt1", "agt2", "agt3", "agt4", "agt5"},
		NumberAlts: 5,
	}
	cmd := commanderagent.NewCommanderAgent(
		id,
		url,
		ballot,
	)

	// send the request
	cmd.DoNewBallot()

	log.Println("démarrage des clients...")
	// id2 := fmt.Sprintf("id%02d", 1)
	agt := voteragent.NewRestClientAgent(
		"ag1",
		url,
		[]models.Alternative{1, 2, 3, 4, 5},
		"vote1",
	)

	res, err := agt.DoRequest()
	fmt.Println("res : ", res)
	fmt.Println("err : ", err)

	// return code should be ServiceNotAvailable
	if err.Error() != "[503] 503 Service Unavailable" {
		t.Error("Error should be ServiceNotAvailable and is", err.Error())
	}

	// ----------------- TEST NOT IMPLEMENTED -----------------
	// we don't test for 501 Not Implemented as it is already tested in TestNewBallot (commander agent would have already caught it)

	// ----------------- TEST FORBIDDEN & 200 OK -----------------
	// test for agent trying to vote again (see by his agent id)

	ballot = types.Ballot{
		Rule: "copeland",
		Deadline: func() string {
			return time.Now().Add(10 * time.Minute).Format("2006-01-02 15:04:05")
		}(),
		VoterIDs:   []string{"agt1", "agt2", "agt3", "agt4", "agt5"},
		NumberAlts: 5,
	}

	cmd = commanderagent.NewCommanderAgent(
		id,
		url,
		ballot,
	)

	// send the request
	cmd.DoNewBallot()

	agt = voteragent.NewRestClientAgent(
		"agt1",
		url,
		[]models.Alternative{1, 2, 3, 4, 5},
		"vote2",
	)

	_, err = agt.DoRequest()

	// return code should be OK
	if err != nil {
		t.Error("Error should be nil")
	}

	_, err = agt.DoRequest()

	// return code should be Forbidden
	if err.Error() != "[403] 403 Forbidden" {
		t.Error("Error should be Forbidden and is", err.Error())
	}

	// ----------------- TEST BAD REQUEST -----------------

	// we are going to test two things here:
	// 1. preference invalid
	// 1.1 preference invalid because it is not a permutation (not all alternatives are unique and present)
	// 1.2 not the same number of alternatives as the ballot number of alternatives
	// 2. Empty fields
	// 2.1 Options is empty and we are using stv/approval
	// 2.2 Options is empty and we are using something other than stv/approval => expect 200 OK
	// 2.3 Options is not empty and we are using stv/approval => expect 200 OK
	// 2.4 Options has length != from length of preferences and we are using approval => expect Bad Request
	// 2.5 Options has length != from length of preferences and we are using STV => expect 200 OK

	// ----------------- TEST 1.1 -----------------

	// preference is not a permutation

	ballot = types.Ballot{
		Rule: "copeland",
		Deadline: func() string {
			return time.Now().Add(2 * time.Minute).Format("2006-01-02 15:04:05")
		}(),
		VoterIDs:   []string{"agt1", "agt2", "agt3", "agt4", "agt5"},
		NumberAlts: 5,
	}
	cmd = commanderagent.NewCommanderAgent(
		id,
		url,
		ballot,
	)

	// send the request
	_, err = cmd.DoNewBallot()

	// return code should be OK
	if err != nil {
		t.Error("Error should be nil")
	}

	agt = voteragent.NewRestClientAgent("ag1", url2, []models.Alternative{1, 2, 3, 4, 4}, "vote3")
	_, err = agt.DoRequest()

	// return code should be BadRequest
	if err.Error() != "[400] 400 Bad Request" {
		t.Error("Error should be BadRequest and is", err.Error())
	}

	// ----------------- TEST 1.2 -----------------

	// not the same number of alternatives as the ballot number of alternatives

	agt = voteragent.NewRestClientAgent("ag1", url2, []models.Alternative{1, 2, 3, 4, 5, 6}, "vote3")
	_, err = agt.DoRequest()

	// return code should be BadRequest
	if err.Error() != "[400] 400 Bad Request" {
		t.Error("Error should be BadRequest and is", err.Error())
	}

	// ----------------- TEST 2.1 -----------------

	// Options is empty and we are using stv/approval
	ballot = types.Ballot{
		Rule: "approval",
		Deadline: func() string {
			return time.Now().Add(2 * time.Minute).Format("2006-01-02 15:04:05")
		}(),
		VoterIDs:   []string{"agt1", "agt2", "agt3", "agt4", "agt5"},
		NumberAlts: 5,
	}

	cmd = commanderagent.NewCommanderAgent(
		id,
		url,
		ballot,
	)

	// send the request
	_, err = cmd.DoNewBallot()

	agt = voteragent.NewRestClientAgent("ag1", url2, []models.Alternative{1, 2, 3, 4, 5}, "vote4")
	_, err = agt.DoRequest()

	// return code should be BadRequest
	if err.Error() != "[400] 400 Bad Request" {
		t.Error("Error should be BadRequest and is", err.Error())
	}

	// ----------------- TEST 2.3 -----------------

	// Options is not empty and we are using stv/approval => expect 200 OK
	ballot = types.Ballot{
		Rule: "stv",
		Deadline: func() string {
			return time.Now().Add(2 * time.Minute).Format("2006-01-02 15:04:05")
		}(),
		VoterIDs:   []string{"agt1", "agt2", "agt3", "agt4", "agt5"},
		NumberAlts: 5,
	}

	cmd = commanderagent.NewCommanderAgent(
		id,
		url,
		ballot,
	)

	// send the request
	_, err = cmd.DoNewBallot()

	agt = voteragent.NewRestClientAgent("ag1", url2, []models.Alternative{1, 2, 3, 4, 5}, "vote5")
	agt.AddOptions([]int{1, 2, 3, 4, 5})
	_, err = agt.DoRequest()

	// return code should be OK
	if err != nil {
		t.Error("Error should be nil")
	}

	// ----------------- TEST 2.4 -----------------

	// Options has length != from length of preferences and we are using approval => expect Bad Request

	ballot = types.Ballot{
		Rule: "approval",
		Deadline: func() string {
			return time.Now().Add(2 * time.Minute).Format("2006-01-02 15:04:05")
		}(),
		VoterIDs:   []string{"agt1", "agt2", "agt3", "agt4", "agt5"},
		NumberAlts: 5,
	}

	cmd = commanderagent.NewCommanderAgent(
		id,
		url,
		ballot,
	)

	// send the request
	_, err = cmd.DoNewBallot()

	agt = voteragent.NewRestClientAgent("ag1", url2, []models.Alternative{1, 2, 3, 4, 5}, "vote6")
	agt.AddOptions([]int{1})
	_, err = agt.DoRequest()

	// return code should be BadRequest
	if err.Error() != "[400] 400 Bad Request" {
		t.Error("Error should be BadRequest and is", err.Error())
	}

	// ----------------- TEST 2.5 -----------------

	// Options has length != from length of preferences and we are using STV => expect 200 OK

	ballot = types.Ballot{
		Rule: "stv",
		Deadline: func() string {
			return time.Now().Add(2 * time.Minute).Format("2006-01-02 15:04:05")
		}(),
		VoterIDs:   []string{"agt1", "agt2", "agt3", "agt4", "agt5"},
		NumberAlts: 5,
	}

	cmd = commanderagent.NewCommanderAgent(
		id,
		url,
		ballot,
	)

	// send the request
	_, err = cmd.DoNewBallot()

	agt = voteragent.NewRestClientAgent("ag1", url2, []models.Alternative{1, 2, 3, 4, 5}, "vote7")
	agt.AddOptions([]int{1})

	_, err = agt.DoRequest()

	// return code should be OK

	if err != nil {
		t.Error("Error should be nil")
	}

}

func TestDoResult(t *testing.T) {

	//  ----------------- TEST OK  -----------------
	const n = 6
	const url1 = ":8080"
	const url2 = "http://localhost:8080"

	clAgts := make([]voteragent.RestClientAgent, 0, n)
	servAgt := ballotagent.NewRestServerAgent(url1)

	log.Println("démarrage du serveur...")
	go servAgt.Start()

	log.Println("démarrage du commandant...")
	id := "commander"
	url := url2
	ballot := types.Ballot{
		Rule: "copeland",
		Deadline: func() string {
			return time.Now().Add(300 * time.Millisecond).Format("2006-01-02 15:04:05")
		}(),
		VoterIDs:   []string{"agt1", "agt2", "agt3", "agt4", "agt5", "agt6"},
		NumberAlts: 5,
	}

	cmd := commanderagent.NewCommanderAgent(
		id,
		url,
		ballot,
	)

	cmd.DoNewBallot()

	log.Println("démarrage des clients...")
	for i := 0; i < n; i++ {
		id := fmt.Sprintf("id%02d", i)
		agt := voteragent.NewRestClientAgent(id, url2, utils.RandomPreference(5), "vote1")
		if ballot.Rule == "approval" || ballot.Rule == "stv" {
			var options []int = []int{3, 2, 4, 1, 3}
			agt.AddOptions(options)
		}
		clAgts = append(clAgts, *agt)
	}

	for _, agt := range clAgts {
		agt.DoRequest()
	}

	time.Sleep(10 * time.Second)

	log.Println("Récupération du résultat")

	_, err := cmd.DoResult("vote1")

	if err != nil {
		t.Error("Error should be nil")
	}

	// ----------------- TEST TOO EARLY  -----------------

	ballot = types.Ballot{
		Rule: "borda",
		Deadline: func() string {
			return time.Now().Add(100 * time.Millisecond).Format("2006-01-02 15:04:05")
		}(),
		VoterIDs:   []string{"agt1", "agt2", "agt3", "agt4", "agt5"},
		NumberAlts: 5,
	}

	cmd = commanderagent.NewCommanderAgent(
		id,
		url,
		ballot,
	)

	// send the request
	_, err = cmd.DoNewBallot()

	agt := voteragent.NewRestClientAgent("ag1", url2, []models.Alternative{1, 2, 3, 4, 5}, "vote2")
	agt.DoRequest()
	agt = voteragent.NewRestClientAgent("ag2", url2, []models.Alternative{1, 2, 3, 4, 5}, "vote2")
	agt.DoRequest()
	agt = voteragent.NewRestClientAgent("ag3", url2, []models.Alternative{1, 2, 3, 4, 5}, "vote2")
	agt.DoRequest()
	agt = voteragent.NewRestClientAgent("ag4", url2, []models.Alternative{1, 2, 3, 4, 5}, "vote2")
	agt.DoRequest()
	agt = voteragent.NewRestClientAgent("ag5", url2, []models.Alternative{1, 2, 3, 4, 5}, "vote2")
	agt.DoRequest()

	res, err := cmd.DoResult("vote2")
	fmt.Println(res)

	// return code should be 425 Too Early
	if err.Error() != "[425] 425 Too Early" {
		t.Error("Error should be Too Early and is", err.Error())
	}

	// ----------------- TEST NOT FOUND  -----------------

	_, err = cmd.DoResult("vote3")

	// return code should be 404 Not Found
	if err.Error() != "[404] 404 Not Found" {
		t.Error("Error should be NotFound and is", err.Error())
	}
}
