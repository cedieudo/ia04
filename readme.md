# IA04 - Systèmes multi-agents - API RESTful Bureau de vote

## Auteurs
Yves Arrata & César Dieudonné

## Présentation
Ce projet est un projet de cours de l'UTC. Il a pour but de mettre en place un système multi-agents permettant de gérer un bureau de vote.

## Installation
```golang
go install gitlab.utc.fr/cedieudo/ia04@v1.0.0
cd $HOME/go/pkg/mod/gitlab.utc.fr/cedieudo/ia04@v1.0.0
go run agt/launch-all-rest-agents/launch-all-agents.go
```

## Utilisation
Un exemple d'utilisation est disponible dans le fichier `agt/launch-all-rest-agents/launch-all-agents.go`.
On commence par lancer le serveur de vote, puis le commandant, puis les agents votants.
Le commandant gère le scrutin qui permet de définir la méthode de vote, le nombre d'alternatives, le nombre de votants, et la date limite de vote.
Les agents votants sont des agents qui votent pour une alternative parmi un ensemble d'alternatives.
Le commandant récupère le résultat du vote une fois la date limite de vote dépassée (d'où le time.Sleep(1 * time.Second)).
Le résultat est affiché dans la console.

## API
Le serveur de vote est un serveur RESTful qui permet de gérer un scrutin.
Il est accessible à l'adresse `http://localhost:8080`.

### Nouveau scrutin
Il est possible de créer un scrutin, de voter, et de récupérer le résultat du vote.
On crée un scrutin avec la route `POST /new_ballot` en précisant la règle de vote, le nombre d'alternatives, les votants, et la date limite de vote:
```json
{
    "Rule": "copeland",
    "Deadline": "2020-12-31 23:59:59",
    "VoterIDs": ["agt1", "agt2", "agt3", "agt4", "agt5", "agt6"],
    "NumberAlts": 5
}

```

### Voter

Pour voter, on utilise la route `POST /vote` en précisant l'identifiant de l'agent votant, l'identifiant du scrutin, et le vote:
```json
{
    "AgentID": "agt1",
    "VoteID": "vote1",
    "Preference": [1, 2, 3, 4, 5],
    "Options": [3, 3, 3, 3, 3] // optionnel, à préciser pour les règles de vote "approval" et "stv"
}
```

### Récupérer le résultat

Pour récupérer le résultat du vote, on utilise la route `GET /result/{voteID}`. Le résultat est affiché sous forme d'objet JSON.

## Méthodes implémentées
Les méthodes implémentées sont les suivantes :
- Copeland
- Approval
- STV
- Borda
- Condorcet
- Majorité simple

## Tests

Les tests sont disponibles dans `test/test_test.go`.
Ils permettent de tester les comportements des agents ainsi que les codes d'erreur renvoyés par le serveur de vote.
