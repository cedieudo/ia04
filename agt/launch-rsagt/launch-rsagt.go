package main

import (
	"fmt"

	ras "gitlab.utc.fr/cedieudo/ia04/agt/ballotagent"
)

func main() {
	server := ras.NewRestServerAgent(":8080")
	server.Start()
	fmt.Scanln()
}
