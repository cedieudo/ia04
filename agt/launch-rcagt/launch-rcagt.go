package main

import (
	"fmt"

	"gitlab.utc.fr/cedieudo/ia04/agt/voteragent"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/models"
)

func main() {
	ag := voteragent.NewRestClientAgent("id1", "http://localhost:8080", []models.Alternative{1, 2, 3, 4, 5}, "vote1")
	ag.Start()
	fmt.Scanln()
}
