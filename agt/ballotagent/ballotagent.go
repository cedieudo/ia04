package ballotagent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"sync"
	"time"

	rad "gitlab.utc.fr/cedieudo/ia04/agt/types"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/approval"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/borda"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/condorcet"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/copeland"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/majoritesimple"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/models"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/stv"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/utils"
)

var Store map[string]rad.StorableBallot = make(map[string]rad.StorableBallot)

var implemented = []string{
	"majority",
	"approval",
	"borda",
	"stv",
	"condorcet",
	"dodgson",
	"young",
	"copeland",
}

type RestServerAgent struct {
	sync.Mutex
	id       string
	reqCount int
	addr     string
	// exclusion mutuelle pour la mise à jour de reqCount car accès concurrents pour les requêtes
}

func getBallotById(ballotId string) rad.StorableBallot {
	return Store[ballotId]
}

func existsIn(m map[string]rad.StorableBallot, key string) bool {
	_, ok := m[key]
	return ok
}

func NewRestServerAgent(addr string) *RestServerAgent {
	return &RestServerAgent{id: addr, addr: addr}
}

// Test de la méthode
func (rsa *RestServerAgent) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "method %q not allowed", r.Method)
		return false
	}
	return true
}

func (*RestServerAgent) decodeRequest(r *http.Request) (req rad.Request, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (*RestServerAgent) decodeBallot(r *http.Request) (req rad.Ballot, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (rsa *RestServerAgent) doNewBallot(w http.ResponseWriter, r *http.Request) {
	// manage the reception of new ballot to put it in the profile and stuff so that doVote
	// can act upon that info

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	rsa.Lock()
	rsa.reqCount++
	rsa.Unlock()

	// décodage de la requête
	req, err := rsa.decodeBallot(r)
	if err != nil || utils.EmptyField(req) {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if !utils.Contains(implemented, req.Rule) {
		w.WriteHeader(http.StatusNotImplemented)
		return
	}

	// traitement de la requête
	var resp rad.BallotResponse = rad.BallotResponse{
		BallotID: fmt.Sprint(rsa.reqCount),
	}

	ballotId := "vote" + fmt.Sprint(rsa.reqCount)
	Store[ballotId] = rad.StorableBallot{
		NbVoters:    len(req.VoterIDs),
		Profile:     make(models.Profile, 0),
		Rule:        req.Rule,
		Deadline:    req.Deadline,
		NumberAlts:  req.NumberAlts,
		BallonEnded: false,
		Voters:      make(map[string]bool),
	}

	w.WriteHeader(http.StatusCreated)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

func (rsa *RestServerAgent) doVote(w http.ResponseWriter, r *http.Request) {

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err := rsa.decodeRequest(r)
	currentBallot := getBallotById(req.VoteID)

	validVote, msg := utils.ValidVote(req, currentBallot, currentBallot.Profile)

	if err != nil || !validVote { // invalid vote should check for empty fields in request struct or invalid preference (not a permutation, not same number of alternatives, etc.)
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Valid Vote : %v %v\nErr : %v\n currentBallot: %v\n", validVote, msg, err, currentBallot)
		return
	}

	if utils.HasVoted(currentBallot, req.AgentID) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	if utils.DeadlineReached(currentBallot.Deadline) {
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	if len(currentBallot.Profile) != currentBallot.NbVoters && !utils.DeadlineReached(currentBallot.Deadline) {
		tmp := Store[req.VoteID]
		tmp2 := tmp.Voters
		tmp2[req.AgentID] = true
		tmp3 := tmp.Profile
		tmp3 = append(tmp3, req.Preference)
		tmp.Profile = tmp3
		tmp.Voters = tmp2
		Store[req.VoteID] = tmp
	}

	if len(currentBallot.Profile)+1 == currentBallot.NbVoters || utils.DeadlineReached(currentBallot.Deadline) {
		// on applique le vote avec les swf factories, sans égalité
		tb := utils.TieBreakFactory(utils.RandomPreference(currentBallot.NumberAlts))
		var SCF func(models.Profile) (models.Alternative, error)
		var SCFOptions func(models.Profile, []int) (models.Alternative, error)
		var newSCF models.Alternative
		var err error
		var optionsUsed bool = false

		if currentBallot.Rule == "borda" {
			SCF = utils.SCFFactory(borda.BordaSCF, tb)
		} else if currentBallot.Rule == "majority" {
			SCF = utils.SCFFactory(majoritesimple.MajoritySCF, tb)
		} else if currentBallot.Rule == "stv" {
			SCFOptions = utils.SCFFactoryOptions(stv.StvSCF, tb)
			optionsUsed = true
		} else if currentBallot.Rule == "approval" {
			SCFOptions = utils.SCFFactoryOptions(approval.ApprovalSCF, tb)
			optionsUsed = true
		} else if currentBallot.Rule == "copeland" {
			SCF = utils.SCFFactory(copeland.CopelandSCF, tb)
		} else if currentBallot.Rule == "condorcet" {
			SCF = utils.SCFFactory(condorcet.CondorcetWinner, tb)
		} else {
			w.WriteHeader(http.StatusNotImplemented)
			return
		}

		if !optionsUsed {
			newSCF, err = SCF(currentBallot.Profile)
		} else {
			newSCF, err = SCFOptions(currentBallot.Profile, req.Options)
		}

		if err != nil {
			log.Println(err)
		}

		tmp := Store[req.VoteID]
		tmp.BallonEnded = true
		tmp2 := tmp.Voters
		tmp2[req.AgentID] = true
		tmp.Voters = tmp2
		tmp.Winner = newSCF
		Store[req.VoteID] = tmp
	}

	w.WriteHeader(http.StatusOK)

}

func (rsa *RestServerAgent) doWinner(w http.ResponseWriter, r *http.Request) {
	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// url passed in the request has form /result/<ballotId>
	// we want to extract the ballotId that is a string
	paramBallotId := strings.Split(r.URL.Path, "/")[2]
	currentBallot := getBallotById(paramBallotId)

	if !existsIn(Store, paramBallotId) {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	if !currentBallot.BallonEnded || !utils.DeadlineReached(currentBallot.Deadline) {
		w.WriteHeader(http.StatusTooEarly)
		return
	}

	// traitement de la requête
	var resp rad.WinnerResponse = rad.WinnerResponse{
		Winner: currentBallot.Winner,
	}

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

func (rsa *RestServerAgent) doReqcount(w http.ResponseWriter, r *http.Request) {
	if !rsa.checkMethod("GET", w, r) {
		return
	}

	w.WriteHeader(http.StatusOK)
	rsa.Lock()
	defer rsa.Unlock()
	serial, _ := json.Marshal(rsa.reqCount)
	w.Write(serial)
}

func (rsa *RestServerAgent) Start() {
	// création du multiplexer
	mux := http.NewServeMux()
	mux.HandleFunc("/vote", rsa.doVote)
	mux.HandleFunc("/reqcount", rsa.doReqcount)
	mux.HandleFunc("/new_ballot", rsa.doNewBallot)

	// handle /result/1 or /result/2 or /result/3 ... etc
	mux.HandleFunc("/result/", rsa.doWinner)

	// création du serveur http
	s := &http.Server{
		Addr:           rsa.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	// lancement du serveur
	log.Println("Listening on", rsa.addr)
	go log.Fatal(s.ListenAndServe())
}
