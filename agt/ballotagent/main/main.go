// TO DELETE, JUST FOR PERSONAL TESTS

package main

import (
	"fmt"

	"gitlab.utc.fr/cedieudo/ia04/agt/types"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/utils"
)

func main() {
	// var profile models.Profile = models.Profile{
	// 	[]models.Alternative{1, 2, 3, 4},
	// 	[]models.Alternative{1, 2, 3, 4},
	// 	[]models.Alternative{1, 2, 3, 4},
	// 	[]models.Alternative{1, 2, 3, 4},
	// 	[]models.Alternative{1, 2, 3, 4},
	// 	[]models.Alternative{2, 3, 4, 1},
	// 	[]models.Alternative{2, 3, 4, 1},
	// 	[]models.Alternative{2, 3, 4, 1},
	// 	[]models.Alternative{2, 3, 4, 1},
	// 	[]models.Alternative{4, 3, 1, 2},
	// 	[]models.Alternative{4, 3, 1, 2},
	// 	[]models.Alternative{4, 3, 1, 2},
	// }

	// tbSWF := utils.TieBreakFactory([]models.Alternative{1, 2, 3, 4, 5})
	// SWF := utils.SWFFactory(stv.StvSWF, tbSWF)
	// newSWF, err := SWF(profile)
	// fmt.Println(newSWF, err)

	// majoritySCF := utils.SCFFactoryOptions(stv.StvSCF, tbSWF)
	// newMajoritySCF, err := majoritySCF(profile, []int{3})
	// fmt.Println(newMajoritySCF, err)
	ballot := types.Ballot{
		Rule:       "stv",
		Deadline:   "2020-12-31T23:59:59Z",
		NumberAlts: 5,
	}

	fmt.Println(utils.EmptyField(ballot))

}
