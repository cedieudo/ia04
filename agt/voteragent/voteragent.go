package voteragent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	rad "gitlab.utc.fr/cedieudo/ia04/agt/types"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/models"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/utils"
)

type RestClientAgent struct {
	id         string
	voteID     string
	url        string
	preference []models.Alternative
	options    []int `json:"options, omitempty"`
}

type AgentI interface {
	Equal(ag RestClientAgent) bool
	DeepEqual(ag RestClientAgent) bool
	Clone() RestClientAgent
	String() string
	Prefers(a models.Alternative, b models.Alternative)
	Start()
}

func (ag RestClientAgent) Equal(ag2 RestClientAgent) bool {
	return ag.id == ag2.id
}

func (ag RestClientAgent) DeepEqual(ag2 RestClientAgent) bool {
	return ag.id == ag2.id && ag.voteID == ag2.voteID && ag.url == ag2.url && utils.Equals(ag.preference, ag2.preference) && utils.Equals(ag.options, ag2.options)
}

func (ag RestClientAgent) Clone() RestClientAgent {
	return RestClientAgent{
		id:         ag.id,
		voteID:     ag.voteID,
		url:        ag.url,
		preference: ag.preference,
		options:    ag.options,
	}
}

func (ag RestClientAgent) String() string {
	return fmt.Sprintf("Agent %s", ag.id)
}

func (ag RestClientAgent) Prefers(a models.Alternative, b models.Alternative) bool {
	return utils.Rank(a, ag.preference) < utils.Rank(b, ag.preference)
}

func (rca *RestClientAgent) AddOptions(options []int) {
	rca.options = options
}

func NewRestClientAgent(
	id string,
	url string,
	preference []models.Alternative,
	voteID string,
) *RestClientAgent {
	return &RestClientAgent{
		id:         id,
		url:        url,
		preference: preference,
		voteID:     voteID,
	}
}

func (rca *RestClientAgent) treatResponse(r *http.Response) models.Alternative {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp rad.Response
	json.Unmarshal(buf.Bytes(), &resp)

	return resp.Result
}

func (rca *RestClientAgent) DoRequest() (res models.Alternative, err error) {
	req := rad.Request{
		AgentID:    rca.id,
		VoteID:     rca.voteID,
		Preference: rca.preference,
		Options:    rca.options,
	}

	// sérialisation de la requête
	url := rca.url + "/vote"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}

	if resp.StatusCode == http.StatusOK {
		fmt.Println("Vote créé")
	}

	if resp.StatusCode == http.StatusBadRequest {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}

	if resp.StatusCode == http.StatusForbidden {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		fmt.Println("Le vote existe déjà !")
		return
	}

	if resp.StatusCode == http.StatusNotImplemented {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		fmt.Println("Rule not implemented")
		return
	}

	if resp.StatusCode == http.StatusServiceUnavailable {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		fmt.Println("Deadline dépassée !")
		return
	}

	res = rca.treatResponse(resp)

	return
}

func (rca *RestClientAgent) Start() {
	log.Printf("démarrage de %s", rca.id)
	_, err := rca.DoRequest()

	if err != nil {
		log.Fatal(rca.id, "error:", err.Error())
	} else {
		log.Printf("[%s] %d\n", rca.id, rca.preference)
	}
}
