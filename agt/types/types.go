package types

import "gitlab.utc.fr/cedieudo/ia04/comsoc/models"

type Request struct {
	AgentID    string
	VoteID     string
	Preference []models.Alternative `json:"preference"`
	Options    []int                `json:"options"`
}

type Response struct {
	Result models.Alternative `json:"res"`
}

type Ballot struct {
	Rule       string
	Deadline   string
	VoterIDs   []string
	NumberAlts int
}

type BallotResponse struct {
	BallotID string
}

type WinnerResponse struct {
	Winner models.Alternative
}

type StorableBallot struct {
	NbVoters    int
	Rule        string
	Deadline    string
	NumberAlts  int
	Profile     models.Profile
	BallonEnded bool
	Winner      models.Alternative
	Voters      map[string]bool // {"ag1": true, "ag2": false, "ag3": true, ...}
	// true if the agent has voted, false otherwise
}
