package main

import (
	"fmt"
	"log"
	"sync"
	"time"

	"gitlab.utc.fr/cedieudo/ia04/agt/ballotagent"
	"gitlab.utc.fr/cedieudo/ia04/agt/commanderagent"
	"gitlab.utc.fr/cedieudo/ia04/agt/types"
	"gitlab.utc.fr/cedieudo/ia04/agt/voteragent"
	"gitlab.utc.fr/cedieudo/ia04/comsoc/utils"
)

func main() {
	const n = 6
	const url1 = ":8080"
	const url2 = "http://localhost:8080"

	clAgts := make([]voteragent.RestClientAgent, 0, n)
	servAgt := ballotagent.NewRestServerAgent(url1)

	log.Println("démarrage du serveur...")
	go servAgt.Start()

	log.Println("démarrage du commandant...")
	id := "commander"
	url := url2
	ballot := types.Ballot{
		Rule: "copeland",
		Deadline: func() string {
			return time.Now().Add(300 * time.Millisecond).Format("2006-01-02 15:04:05")
		}(),
		VoterIDs:   []string{"agt1", "agt2", "agt3", "agt4", "agt5", "agt6"},
		NumberAlts: 5,
	}

	var wg1 sync.WaitGroup
	cmd := commanderagent.NewCommanderAgent(
		id,
		url,
		ballot,
	)
	wg1.Add(1)
	go func() {
		defer wg1.Done()
		cmd.Start()
	}()

	wg1.Wait()

	log.Println("démarrage des clients...")
	for i := 0; i < n; i++ {
		id := fmt.Sprintf("id%02d", i)
		agt := voteragent.NewRestClientAgent(id, url2, utils.RandomPreference(5), "vote1")
		if ballot.Rule == "approval" || ballot.Rule == "stv" {
			var options []int = []int{3, 2, 4, 1, 3}
			agt.AddOptions(options)
		}
		clAgts = append(clAgts, *agt)
	}

	var wg2 sync.WaitGroup

	for _, agt := range clAgts {
		wg2.Add(1)
		go func(agt voteragent.RestClientAgent) {
			defer wg2.Done()
			agt.Start()
		}(agt)
	}

	wg2.Wait()

	var wg3 sync.WaitGroup

	time.Sleep(1 * time.Second)

	log.Println("Récupération du résultat")
	wg3.Add(1)
	go func() {
		defer wg3.Done()
		cmd.GetResult("vote1")
	}()

	wg3.Wait()

	fmt.Scanln()
}
