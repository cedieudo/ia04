package commanderagent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	rad "gitlab.utc.fr/cedieudo/ia04/agt/types"
)

type CommanderAgent struct {
	id     string
	url    string
	ballot rad.Ballot
}

func NewCommanderAgent(id string, url string, ballot rad.Ballot) *CommanderAgent {
	return &CommanderAgent{
		id:     id,
		url:    url,
		ballot: ballot,
	}
}

func (rca *CommanderAgent) treatBallotResponse(r *http.Response) rad.BallotResponse {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp rad.BallotResponse
	json.Unmarshal(buf.Bytes(), &resp)

	return resp
}

func (rca *CommanderAgent) treatWinnerResponse(r *http.Response) rad.WinnerResponse {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp rad.WinnerResponse
	json.Unmarshal(buf.Bytes(), &resp)

	return resp
}

func (rca *CommanderAgent) DoNewBallot() (res rad.BallotResponse, err error) {
	req := rad.Ballot{
		Rule:       rca.ballot.Rule,
		Deadline:   rca.ballot.Deadline,
		VoterIDs:   rca.ballot.VoterIDs,
		NumberAlts: rca.ballot.NumberAlts,
	}
	// sérialisation de la requête
	url := rca.url + "/new_ballot"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}

	if resp.StatusCode == http.StatusNotImplemented {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		fmt.Println("Rule not implemented")
		return
	}

	if resp.StatusCode == http.StatusCreated {
		fmt.Println("Scrutin crée")
	}

	if resp.StatusCode == http.StatusBadRequest {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		fmt.Println("Il semblerait que le scrutin n'a pas été crée...")
		return
	}

	res = rca.treatBallotResponse(resp)

	return
}

// get result of the vote
func (rca *CommanderAgent) DoResult(id string) (res rad.WinnerResponse, err error) {
	req := rad.BallotResponse{
		BallotID: id,
	}
	// sérialisation de la requête
	url := rca.url + "/result/" + id
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}

	res = rca.treatWinnerResponse(resp)

	if resp.StatusCode == http.StatusOK {
		// write the response in the header
		resp.Header.Set("Winner", fmt.Sprint(res.Winner))
		fmt.Println("Scrutin terminé")
	}

	if resp.StatusCode == http.StatusTooEarly {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		fmt.Println("Scrutin pas encore terminé")
		return
	}

	if resp.StatusCode == http.StatusNotFound {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		fmt.Println("Il semblerait que le résultat n'existe pas...")
		return
	}

	return
}

func (rca *CommanderAgent) GetResult(id string) {
	res, err := rca.DoResult(id)

	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println("Le gagnant de", rca.ballot.Rule, "est", res.Winner)
	}
}

func (rca *CommanderAgent) Start() {
	log.Println("CommanderAgent started")
	res, err := rca.DoNewBallot()

	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println("Ballot n°", res.BallotID, "created")
	}
}
